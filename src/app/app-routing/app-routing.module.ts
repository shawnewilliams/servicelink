import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';

import { Nav1Component } from '../nav1/nav1.component';
import { Nav2Component } from '../nav2/nav2.component';
import { Nav3Component } from '../nav3/nav3.component';


const routes: Routes = [
  { path: 'nav-1', component: Nav1Component },   
  { path: 'nav-2', component: Nav2Component },   
  { path: 'nav-3', component: Nav3Component },   

    { path: '', redirectTo: '/nav-1', pathMatch: 'full' }
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    RouterModule.forRoot(routes)
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
