import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataService {

  data;

  constructor() { 
    this.data = [{category: 'Category 1'}, {category: 'Category 2'}, {category: 'Category 3'}, {category: 'Category 4'}, {category: 'Category 5'}, {category: 'Category 6'}];
    if (!this.getData()) {
      this.saveData(this.data);
    } else {
      this.data = this.getData();
    }
  }

  getData() {
    return JSON.parse(localStorage.getItem('categories'));
  }

  saveData(data) {
    localStorage.setItem('categories', JSON.stringify(data))
  }
}
