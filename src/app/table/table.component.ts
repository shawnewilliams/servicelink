import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators } from '@angular/forms';
import { DataService } from '../data.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})
export class TableComponent implements OnInit {

  tableData: Array<any>;
  showInput: boolean = false;

  startY: number;
  endY: number;
  index: number;

  categoryInput = this.fb.group({
    category: ['', Validators.required],
  });

  constructor( private fb: FormBuilder, private dataService: DataService ) { }
  
  
  ngOnInit() {
    this.tableData = this.dataService.getData();
    if(this.tableData.length === 0) {
      this.showInput = true;
    }
  }

  removeItem(index) {
    this.tableData.splice(index, 1);
    this.dataService.saveData(this.tableData);
    if (this.tableData.length === 0) {
      this.showInput = true;
    }
  }

  addNewCategory() {
    this.showInput = !this.showInput
  }

  onSave() {
    if( this.categoryInput.value.category === '') {
      return;
    }
    let data = {category: this.categoryInput.value.category}
    this.tableData.push(data);
    this.categoryInput.controls['category'].setValue('');
    this.showInput = false;
    this.dataService.saveData(this.tableData);
  }

  keyboardSave(e) {
    if(e.keyCode === 13) {
      this.onSave();
    }
  }

  onDrag(e) {
    e.stopPropagation();
    e.preventDefault();
  }

  onDrop(e) {
    e.preventDefault();
    this.endY = e.clientY
    let num =  parseInt(((this.startY - e.clientY) / 48).toString());
    console.log(num)
    this.tableData = this.move(num, this.index, this.tableData);
    this.dataService.saveData(this.tableData);
  }

  onMouseDown(e, index) {
    this.index = index;
    this.startY = e.clientY
  }

  move(num, index, arr) {
    let arrCopy = [...arr];
    let removed =  arrCopy.splice(index, 1)
    let end = arrCopy.splice(index - num)
    arrCopy.push(...removed)
    arrCopy.push(...end)
    return arrCopy;
  }

}
